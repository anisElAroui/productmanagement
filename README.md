# Symfony Stack Environment

This project is made with a Docker stack to install the Symfony app ProductManagement on any system supporting Docker.

## 1. Docker
You need Docker on a Linux, Windows, or MacOS system.

###Important note :
If you already have a running instance of nginx listening on port 80, be sure to switch this service off to allow your brand new container to use it.
```
service  stop  nginx
```

###
Now, you just have to open a terminal at the root of dockerStack directory, then run :
```
docker-compose build
```

###
This command will build everything we need from the “docker-compose.yml”.

Once your app is built. You can run :
```
docker-compose up -d
```

###
To stop your containers, use :
```
docker-compose stop
```

## 2. Symfony
Now you can run the Symfony commands for building your application through your Docker container:
```
docker exec -it tekyn_php bash
```

And voilà! You can now see your application running on Docker and available on : http://localhost/ (or your virtual host)



###
#### Adding a custom host
You can add a custom host in your system so the web server will respond to `http://productmanagement.local` request url.
To add a host in the system Host file, add this line :
```
127.0.0.1 productmanagement.local
```
on Linux or Mac: `/etc/hosts`, \
on Windows : `%WinDir%\System32\Drivers\Etc\hosts`
